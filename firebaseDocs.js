// comenzamos inicializando el codigo que provee firebase para conectarnos a la db de nuestra cuenta
var config = {
     apiKey: "AIzaSyDRxLlgwIEvtQukOFlz_Er6WlCGeevQGOY",
     authDomain: "experimentos-7cc02.firebaseapp.com",
     databaseURL: "https://experimentos-7cc02.firebaseio.com",
     projectId: "experimentos-7cc02",
     storageBucket: "experimentos-7cc02.appspot.com",
     messagingSenderId: "1081518740422"
};
  firebase.initializeApp(config);

  // instanciamos e inicializamos la base de datos en la variable db
  var db = firebase.database();

// OPERACIÓN DE ESCRITURA EN LA BASE DE DATOS -----------------------------------------------------------------------------------------------------------------------------------
// primero instanciamos el objeto "interesado" que vamos a escribir en la base de datos

  var interesado = {
    nombre:'aaaaa',
    email:'ssss',
    pais:'pais',
    mensaje:'dddd'
  }

  //Una vez instanciado el objeto, referenciamos la collección en que vamos a escribir, y por medio
  // del metodo push insertamos el nuevo registro, este metodo nos genera una nueva referencia con un id automatico en la db, el id es con base a la fecha así que lo guardará en orden cronológico

  db.ref("users").push(interesado);

  // Existe otro metodo llamado set, con este seteamos el objeto al que hacemos referencia ejemplo:

  db.ref("users").child("-LJQD4xCCjEBGv5VOlE9").set({
    nombre:"nuevo",
    email:"nuevoEmail"
  })
  // lo que acabamos de hacer fue setear(borrar e ingresar nuevamente) la información de un objeto con el identificador que posee child(), dentro de la colección "users"
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------






// OPERACIÓNES DE LECTURA -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// con el metodo ".on", abrimos un canal que permanecerá abierto para escuchar la referencia(collección) deseada,

// y con el evento child added recuperamos la lista de elementos de esa referencia(collección)

  db.ref("users").on("child_added", (snapshot)=>{
    // definimos la variable snapshot en el callback del metodo "child_added", snapshot representará cada elemento por separado de la colección especificada "users", en pocas palabras hacemos un mapeo(bucle, forEach) de la colección

    var datos = snapshot.val();
    // con el metodo ".val()" obtenemos el objeto de forma legible

    var idElement = snapshot.key;
    // para obtener el identificador de cada elemento podemos acceder a el con el indice ".key"
  })


// con el metodo ".once" haremos una sola petición, solo se escuchara 1 sola vez la referencia deseada y no detectaremos cambios posteriores al llamado
// utilizaremos el evento "value" que nos regresa la colección completa en un objeto JSON

  db.ref("users").once('value', snap => {
    // esto nos retorna el objeto JSON en laa variable snap con la colección completa a la que hacemos referencia "users"

    snap.forEach(childSnap=>{
      // Recorremos el objeto JSON con el metodo forEach que nos retorna un objeto individual de cada item de la colección "users" en una variable que denominamos "childSnap"
      console.log(childSnap.val());
      console.log(childSnap.key);
      // Extraemos los datos de cada item
    })
  });


// PODEMOS hacer aplicación con cualquier metodo mencionado anteriormente, enfocado al modelado de datos orientado a objetos de la siguiente forma
//en este ejemeplo recorremos la collección users con el metodo "child_added", modelamos el usuario con los datos obtenidos y lo insertamos en un arreglo llamado usuarios...

var usuarios = [];

db.ref("users").on("child_added", (snapshot)=>{

  var datos = snapshot.val();

  var user = {
    _id:snapshot.key,
    nombre:datos.nombre,
    email:datos.email,
    mensaje:datos.mensaje,
    pais:datos.pais,
  }
  usuarios.push(user);

})

// var userUpdate = usuarios.find(item=>item._id === user_id); metodo para encontrar un dato en un array


// referencia completa sobre trabajo de listas de datos y sus eventos en : https://firebase.google.com/docs/database/web/lists-of-data




  // estas son dos formas de hacer una consulta del id 0 en el objeto categories de la db
  // var db = firebase.database().ref("categories").child("0");
  // var db = firebase.database().ref("categories/"+"0");






// Ver mas información de lectura y escritura
//ver documentación en: https://firebase.google.com/docs/database/web/read-and-write



  function penetrar(){

    var interesado = {
      nombre:'aaaaa',
      email:'ssss',
      pais:'pais',
      mensaje:'dddd'
    }

    var id = 5;
    var bd = firebase.database().ref("users");
    db.push(interesado);
    // db.key(interesado);
  }






  // var adaRankRef = firebase.database().ref('categories/0');
  // adaRankRef.transaction(function(currentRank) {
  //   console.log("este es el proof", currentRank);
  //   return currentRank + 1;
  // });


  // ref
  // .orderByChild('age')
  // .startAt('35')
  // .limitToFirst(3)
  // .once('value', function(s) {
  //   console.log(JSON.stringify(s.val(), null, '  '));
  // }, function(error) {
  //   if(error) console.error(error);
  // })


  // https://firebase.google.com/docs/reference/js/firebase.database.Reference#transaction



// en este ejemplo mostraremos como imprimir los datos con vanillaJS en el DOM
  db.ref("users").on("child_added", (snapshot)=>{
    // console.log('elemento',snapshot.val());
    // console.log('llave elemento',snapshot.key);

    var datos = snapshot.val();
    data.push(datos);

    var users = document.createElement("article");
    var contenido = `<h2>${datos.email}</h2>`;
    users.innerHTML = contenido;
    document.getElementById("usuarios").appendChild(users);
  })
